
local old_sound_play = minetest.sound_play
local old_sound_stop = minetest.sound_stop

-- object for a given handle
local bound_sounds = {}
-- all handles bound to one object
local sound_handles_per_object = {}


-- new sound_play() to remember handle if attached to an object
function minetest.sound_play(spec, parameters)
	local handle = old_sound_play(spec, parameters)
	local obj = parameters.object

	if handle > -1 and obj ~= nil and type(obj:get_pos()) == "table" then
		if sound_handles_per_object[tostring(obj)] == nil then
			sound_handles_per_object[tostring(obj)] = {}
		end
		sound_handles_per_object[tostring(obj)][handle] = handle
		bound_sounds[handle] = obj
	end

	return handle
end


-- new sound_stop() to remove from object mapping
function minetest.sound_stop(handle)
	local obj = bound_sounds[handle]
	if obj ~= nil then
		sound_handles_per_object[tostring(obj)][handle] = nil
		bound_sounds[handle] = nil
	end

	return old_sound_stop(handle)
end

-- stop_all_sounds() for stopping all sounds attached to an object
function minetest.stop_all_sounds(object)
	assert(object ~= nil)
	local handles = sound_handles_per_object[tostring(object)]
	if handles ~= nil then
		for _, handle in pairs(handles) do
			minetest.sound_stop(handle)
		end
	end
end

-- cleanup_object() stops all attached sounds before destroying the object.
-- call this instead of object:remove() henceforth!
function minetest.cleanup_object(object)
	minetest.stop_all_sounds(object)
	object:remove()
end
