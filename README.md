
# Sound Cleanup mod for Minetest

This is a utility mod for minetest to work around a bug in the engine itself.

## The Bug

Minetest mods can play sounds that are attached to an object, meaning every time the object position is updated, the audio source position is updated, too (at least something like this). When the object is destroyed with `object:remove()`, the sounds are not also removed, and continue playing on their last position until they end. Which looped sounds never do.

## The Workaround

To fix this annoyance, this mod overrides `minetest.sound_play()` and `minetest.sound_stop()` to keep book about which sounds are played attached to which objects. Then it provides the new **`minetest.cleanup_object()`** to stop all sounds attached to that object before removing it. This means that for your mod to take advantage of this, **you should not call `object:remove()`** manually anymore, but use `minetest.cleanup_object()`.

This mod also provides `minetest.stop_all_sounds()` to stop all sounds attached to an object for convenience.
